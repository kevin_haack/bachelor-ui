package de.haack.whs.bachelor.ui;

import java.util.prefs.BackingStoreException;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;

/**
 *
 * @author Kevin Haacks
 */
public class PreferencesManager {

    /* ******************************************
     * keys
     * ****************************************** */
    public static final String KEY_PYTHON_PATH = "pythonPath";
    public static final String KEY_GENERATE_PATH = "generatePath";
    public static final String KEY_SCRIPT_PATHS = "scriptPaths";
    public static final String KEY_INCLUDE_JAVA_GATEWAY = "includeJavaGateway";

    private static final String PREFERENCES_PATH = "/haack/whs/bachelor";
    private static final String UNDEFINED = "...undefined...";

    private final Preferences preferences;

    public PreferencesManager() {
        this.preferences = Preferences.userRoot().node(PREFERENCES_PATH);
    }

    public void remove(String key) {
        this.preferences.remove(key);
    }

    public boolean contains(String key) {
        try {
            String[] keys = preferences.keys();
            boolean found = false;

            int i = 0;
            while (!found
                    && i < keys.length) {
                if (keys[i].equals(key)) {
                    found = true;
                }

                i++;
            }

            return found;
        } catch (BackingStoreException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void addPreferenceChangeListener(PreferenceChangeListener listener) {
        this.preferences.addPreferenceChangeListener(listener);
    }

    public void put(String key, String value) {
        this.preferences.put(key, value);
    }

    public void flush() {
        try {
            this.preferences.flush();
        } catch (BackingStoreException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String get(String key) {
        String value = this.preferences.get(key, UNDEFINED);

        if (value.equals(UNDEFINED)) {
            value = null;
        }

        return value;
    }
}
