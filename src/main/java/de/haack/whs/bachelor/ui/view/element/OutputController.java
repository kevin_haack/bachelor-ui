package de.haack.whs.bachelor.ui.view.element;

import de.haack.fxviewlibrary.view.ViewManager;
import de.haack.fxviewlibrary.view.annotation.ElementController;

/**
 *
 * @author Kevin
 */
@ElementController(
        FXML = "/fxml/element/Output.fxml"
)
public class OutputController extends Connectable {

    private final FunctionController functionController;
    
    public OutputController(FunctionController functionController) {
        ViewManager.loadElementView(OutputController.class, this);
        
        this.functionController = functionController;
    }

    public FunctionController getFunctionController() {
        return functionController;
    }
}
