package de.haack.whs.bachelor.ui.view.element;

import de.haack.bachelor.backend.script.function.Function;
import de.haack.fxviewlibrary.view.ViewManager;
import de.haack.fxviewlibrary.view.annotation.ElementController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller Klasse
 *
 * @author Kevin Haack
 */
@ElementController(
        FXML = "/fxml/element/Function.fxml"
)
public class FunctionController extends AnchorPane {

    @FXML
    private VBox outputs;

    @FXML
    private VBox inputs;

    @FXML
    private Label functionName;

    private final Function function;

    public FunctionController(Function function) {
        ViewManager.loadElementView(FunctionController.class, this);
        this.function = function;
        this.functionName.setText(this.function.getFunctionHeader().getName());

        initOutputs();
        initInputs();
        initContextMenu();

        relocate(this.function.getPosition().x, this.function.getPosition().y);
    }

    public InputController[] getInputs() {
        InputController[] inputArray = new InputController[this.inputs.getChildren().size()];

        for (int i = 0; i < this.inputs.getChildren().size(); i++) {
            inputArray[i] = (InputController) this.inputs.getChildren().get(i);
        }

        return inputArray;
    }

    public OutputController getOutput() {
        return (OutputController) this.outputs.getChildren().get(0);
    }

    private void initOutputs() {
        OutputController output = new OutputController(this);
        this.outputs.getChildren().add(output);
    }

    public Function getFunction() {
        return this.function;
    }

    public int getInputPosition(InputController inputController) {
        return this.inputs.getChildren().indexOf(inputController);
    }

    private void initInputs() {
        for (int i = 0; i < this.function.getParameterCount(); i++) {
            InputController input = new InputController(this);

            this.inputs.getChildren().add(input);
        }
    }

    private void initContextMenu() {
        ContextMenu menu = new ContextMenu();

        MenuItem remove = new MenuItem("Entfernen");
        remove.setOnAction((ActionEvent event) -> {
            ((WorkspaceController) getParent()).removeFunctionController(this);
        });

        menu.getItems().addAll(remove);

        // open contextmenu
        setOnMouseClicked((MouseEvent event) -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                menu.show(getScene().getWindow(), event.getScreenX(), event.getScreenY());
                event.consume();
            }
        });
    }
}
