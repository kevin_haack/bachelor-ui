package de.haack.whs.bachelor.ui.gateway;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import py4j.GatewayConnection;
import py4j.GatewayServerListener;

/**
 *
 * @author Kevin Haack
 */
public class GatewayListener implements GatewayServerListener {

    public final static String STATUS_SERVER_STARTED = "server started";
    public final static String STATUS_SERVER_STOPPED = "server stopped";
    public final static String STATUS_SERVER_ERROR = "server error";
    public final static String STATUS_CONNECTION_STARTED = "connection started";
    public final static String STATUS_CONNECTION_STOPPED = "connection stopped";
    public final static String STATUS_CONNECTION_ERROR = "connection error";

    private final BooleanProperty runningProperty = new SimpleBooleanProperty();
    private final StringProperty statusProperty = new SimpleStringProperty();
    private Exception exception;

    public GatewayListener() {
    }

    public BooleanProperty getRunningProperty() {
        return runningProperty;
    }

    public StringProperty getStatusProperty() {
        return this.statusProperty;
    }

    public Exception getException() {
        return this.exception;
    }

    @Override
    public void connectionError(Exception excptn) {
        this.exception = excptn;
        this.statusProperty.set(STATUS_CONNECTION_ERROR);

    }

    @Override
    public void connectionStarted(GatewayConnection gc) {
        this.statusProperty.set(STATUS_CONNECTION_STARTED);
    }

    @Override
    public void connectionStopped(GatewayConnection gc) {
        this.statusProperty.set(STATUS_CONNECTION_STOPPED);
    }

    @Override
    public void serverError(Exception excptn) {
        this.exception = excptn;
        this.statusProperty.set(STATUS_SERVER_ERROR);
    }

    @Override
    public void serverPostShutdown() {

    }

    @Override
    public void serverPreShutdown() {
    }

    @Override
    public void serverStarted() {
        this.statusProperty.set(STATUS_SERVER_STARTED);
        this.runningProperty.set(true);
    }

    @Override
    public void serverStopped() {
        this.statusProperty.set(STATUS_SERVER_STOPPED);
        this.runningProperty.set(false);
    }

}
