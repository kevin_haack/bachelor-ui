package de.haack.whs.bachelor.ui.view.element;

import java.util.LinkedList;
import java.util.List;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;

/**
 *
 * @author Kevin Haack
 */
public class Connectable extends Pane {
    private List<Parent> getAllParents() {
        List<Parent> parents = new LinkedList<>();
        
        Parent current = getParent();
        
        while(!(current instanceof WorkspaceController)) {
            parents.add(current);
            current = current.getParent();
        }
        
        return parents;
    }
    
    public double getXInWorkspace() {
        double x = getLayoutX();
        
        for(Parent p: getAllParents()) {
            x += p.getLayoutX();
        }
        
        return  x;
    }
    
    public double getYInWorkspace() {
        double y = getLayoutY();
        
        for(Parent p: getAllParents()) {
            y += p.getLayoutY();
        }
        
        return  y;
    }
}
