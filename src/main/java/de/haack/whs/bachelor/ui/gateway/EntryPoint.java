package de.haack.whs.bachelor.ui.gateway;

/**
 *
 * @author Kevin Haack
 */
public class EntryPoint {
    
    private final Communicator communicator = new Communicator();
    
    public Communicator getCommunicator() {
        return this.communicator;
    }
}
