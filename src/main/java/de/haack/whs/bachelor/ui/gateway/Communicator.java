package de.haack.whs.bachelor.ui.gateway;

import java.util.LinkedList;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Kevin Haack
 */
public class Communicator {
    
    private static final String PROGRAM_START = "program start";
    private static final String PROGRAM_END = "program end successfully";
    private static final String EXCEPTION = "exception";
    
    private final ObservableList<String> messages = FXCollections.observableList(new LinkedList<String>());
    private final ObservableList<Integer> errorIds = FXCollections.observableList(new LinkedList<Integer>());

    public ObservableList<String> getMessages() {
        return messages;
    }

    public ObservableList<Integer> getErrorIds() {
        return errorIds;
    }
    
    public void programStart() {
        addMessage(PROGRAM_START);
    }
    
    public void programEnd() {
        addMessage(PROGRAM_END);
    }
    
    public void onException(Integer id, String message) {
        addErrorId(id);
        
        StringBuilder builder = new StringBuilder();
 
        builder.append(EXCEPTION);
        builder.append(": ");
        builder.append(message);
        
        addMessage(builder.toString());
    }
    
    private void addErrorId(Integer id) {
        Platform.runLater(() -> {
            this.errorIds.add(id);
        });
    }
    
    private void addMessage(String message) {
        Platform.runLater(() -> {
            this.messages.add(message);
        });
    }
}
