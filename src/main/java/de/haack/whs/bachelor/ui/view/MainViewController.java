package de.haack.whs.bachelor.ui.view;

import de.haack.bachelor.backend.Program;
import de.haack.bachelor.backend.ProgramFactory;
import de.haack.bachelor.backend.Workspace;
import de.haack.bachelor.backend.script.Evaluable;
import de.haack.bachelor.backend.script.Import;
import de.haack.bachelor.backend.script.LineOfCode;
import de.haack.bachelor.backend.script.Script;
import de.haack.bachelor.backend.script.ScriptLoader;
import de.haack.bachelor.backend.script.function.FunctionHeader;
import de.haack.fxviewlibrary.ResourceManager;
import de.haack.fxviewlibrary.view.AbstractAction;
import de.haack.fxviewlibrary.view.AbstractController;
import de.haack.fxviewlibrary.view.dialog.DialogFactory;
import de.haack.whs.bachelor.ui.PreferencesManager;
import de.haack.whs.bachelor.ui.gateway.EntryPoint;
import de.haack.whs.bachelor.ui.gateway.GatewayListener;
import de.haack.whs.bachelor.ui.view.element.WorkspaceController;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.BindException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.WindowEvent;
import py4j.GatewayServer;

/**
 * FXML Controller Klasse der MainView.
 *
 * @author Kevin Haack
 */
@de.haack.fxviewlibrary.view.annotation.MainViewController(
        FXML = "/fxml/MainView.fxml",
        Title = "MainView.title"
)
public class MainViewController extends AbstractController {

    private final FileChooser workspaceFileChooser = new FileChooser();
    private final FileChooser scriptFileChooser = new FileChooser();
    private final FileChooser generateFileChooser = new FileChooser();
    private final FileChooser pythonFileChooser = new FileChooser();
    private final PreferencesManager preferencesManager = new PreferencesManager();

    private GatewayServer gatewayServer;
    private BooleanProperty gatewayRunning;
    private ObservableList<Integer> errorFunctionIds;

    @FXML
    private TextField generateTextField;

    @FXML
    private TextField pythonTextField;

    @FXML
    private Label fileNameLabel;

    @FXML
    private CheckBox includeJavagateway;

    @FXML
    private CheckBox generateLoop;

    @FXML
    private TableView<FunctionHeader> functionHeaders;

    @FXML
    private TableColumn<FunctionHeader, String> functionNameColumn;

    @FXML
    private TableColumn<FunctionHeader, String> parameterCountColumn;

    @FXML
    private TableColumn<FunctionHeader, String> fileNameColumn;

    @FXML
    private TableColumn<FunctionHeader, String> filePathColumn;

    @FXML
    private WorkspaceController workspaceController;

    @FXML
    private Pane gatewayStatusColor;

    @FXML
    private TextField gatewayStatus;

    @FXML
    private TextField javaGatewayPort;

    @FXML
    private TextField pythonGatewayPort;

    @FXML
    private Button startGateway;

    @FXML
    private ListView<String> gatewayMessagesList;

    private void initFileChooser() {
        this.workspaceFileChooser.setTitle("Select workspace file");
        this.scriptFileChooser.setTitle("Select script file");
        this.generateFileChooser.setTitle("Select script file");
        this.pythonFileChooser.setTitle("Select python");

        this.workspaceFileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        this.scriptFileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        this.generateFileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        this.pythonFileChooser.setInitialDirectory(new File(System.getProperty("user.home")));

        this.workspaceFileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Workspace files (*.ws)", "*.ws"));
        this.scriptFileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Python files (*.py)", "*.py"));
        this.generateFileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Python files (*.py)", "*.py"));
    }

    private void initFunctionHeadersTable() {
        /*
         * init data
         */
        this.functionHeaders.setItems(FXCollections.observableArrayList());

        this.functionNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.parameterCountColumn.setCellValueFactory(new PropertyValueFactory<>("parameterCount"));
        this.fileNameColumn.setCellValueFactory((TableColumn.CellDataFeatures<FunctionHeader, String> param) -> new SimpleObjectProperty<>(param.getValue().getFile().getName()));
        this.filePathColumn.setCellValueFactory(new PropertyValueFactory<>("file"));

        /*
         * context menu 
         */
        ContextMenu contextMenu = new ContextMenu();
        this.functionHeaders.setContextMenu(contextMenu);

        MenuItem removeItem = new MenuItem("Entfernen");
        removeItem.setOnAction((ActionEvent event) -> {
            this.functionHeaders.getItems().remove(this.functionHeaders.getSelectionModel().getSelectedItem());
        });

        contextMenu.getItems().add(removeItem);

        /*
         * load functionheaders
         */
        loadFunctionHeaders();
    }

    private void addFunctionToWorkspace(FunctionHeader functionHeader, double x, double y) {
        this.workspaceController.addFunction(functionHeader, x - 50, y - 50);
    }

    private void openWorkspaceFile(File file) {
        Workspace workspace = Workspace.fromFile(file);
        workspace.setFile(file);

        this.workspaceController.loadWorkspace(workspace);
        this.fileNameLabel.setText(file.getAbsolutePath());
    }

    private void saveWorkspaceFile(File file) {
        Workspace.toFile(this.workspaceController.getWorkspace(), file);
        this.workspaceController.getWorkspace().setFile(file);
        this.fileNameLabel.setText(file.getAbsolutePath());
    }

    private void loadFunctionHeaders() {
        if (this.preferencesManager.contains(PreferencesManager.KEY_SCRIPT_PATHS)) {
            String p = this.preferencesManager.get(PreferencesManager.KEY_SCRIPT_PATHS);

            String[] paths = p.split(";");

            for (String path : paths) {
                if (!path.isEmpty()) {
                    File file = new File(path);

                    if (file.exists()) {
                        openScriptFile(file);
                    }
                }
            }
        }
    }

    private void openScriptFile(File file) {
        ScriptLoader loader = new ScriptLoader();
        Script script = loader.load(file);

        for (FunctionHeader functionHeader : script.getFunctionHeaders()) {
            boolean exist = false;

            for (FunctionHeader f : this.functionHeaders.getItems()) {
                if (f.getName().equals(functionHeader.getName())
                        && f.getFile().equals(functionHeader.getFile())) {
                    exist = true;
                }
            }

            if (!exist) {
                this.functionHeaders.getItems().add(functionHeader);
            }
        }
    }

    @FXML
    public void handleAddScriptFile() {
        File file = this.scriptFileChooser.showOpenDialog(getStage());

        if (null != file) {
            File existDirectory = file.getParentFile();
            this.scriptFileChooser.setInitialDirectory(existDirectory);
            openScriptFile(file);

            // persist path
            StringBuilder builder = new StringBuilder();
            if (this.preferencesManager.contains(PreferencesManager.KEY_SCRIPT_PATHS)) {
                builder.append(this.preferencesManager.get(PreferencesManager.KEY_SCRIPT_PATHS));
                builder.append(";");
            }

            builder.append(file.getAbsolutePath());

            this.preferencesManager.put(PreferencesManager.KEY_SCRIPT_PATHS, builder.toString());
        }
    }

    @FXML
    public void handleDeleteSelected() {
        this.workspaceController.deleteSelected();
    }

    @FXML
    public void handleSelectAll() {
        this.workspaceController.selectAll();
    }

    @FXML
    public void handleNew() {
        this.workspaceController.loadWorkspace(new Workspace());
        this.fileNameLabel.setText("");
    }

    @FXML
    public void handleOpen() {
        File file = this.workspaceFileChooser.showOpenDialog(getStage());

        if (null != file) {
            File existDirectory = file.getParentFile();
            this.workspaceFileChooser.setInitialDirectory(existDirectory);
            openWorkspaceFile(file);
        }
    }

    @FXML
    public void handleSaveAs() {
        File file = this.workspaceFileChooser.showSaveDialog(getStage());

        if (null != file) {
            File existDirectory = file.getParentFile();
            this.workspaceFileChooser.setInitialDirectory(existDirectory);
            saveWorkspaceFile(file);
        }
    }

    @FXML
    public void handleSave() {
        if (null != this.workspaceController.getWorkspace().getFile()) {
            saveWorkspaceFile(this.workspaceController.getWorkspace().getFile());
        } else {
            handleSaveAs();
        }
    }

    @FXML
    public void handleClose() {
        getStage().close();
    }

    @FXML
    public void handleSelectPython() {
        File file = this.pythonFileChooser.showOpenDialog(getStage());

        if (null != file) {
            File existDirectory = file.getParentFile();
            this.pythonFileChooser.setInitialDirectory(existDirectory);
            this.pythonTextField.setText(file.getAbsolutePath());
            this.preferencesManager.put(PreferencesManager.KEY_PYTHON_PATH, file.getAbsolutePath());
        }
    }

    @FXML
    public void handleSelectGenerate() {
        File file = this.generateFileChooser.showSaveDialog(getStage());

        if (null != file) {
            File existDirectory = file.getParentFile();
            this.generateFileChooser.setInitialDirectory(existDirectory);
            this.generateTextField.setText(file.getAbsolutePath());
            this.preferencesManager.put(PreferencesManager.KEY_GENERATE_PATH, file.getAbsolutePath());
        }
    }

    @FXML
    public void handleGenerateRun() {
        if (!this.pythonTextField.getText().isEmpty()) {
            if (!this.generateTextField.getText().isEmpty()) {
                generate();
                run();
            } else {
                handleSelectGenerate();
            }
        } else {
            handleSelectPython();
        }
    }

    @FXML
    public void handleGenerate() {
        if (!this.pythonTextField.getText().isEmpty()) {
            if (!this.generateTextField.getText().isEmpty()) {
                generate();
            } else {
                handleSelectGenerate();
            }
        } else {
            handleSelectPython();
        }
    }

    @FXML
    public void handleStartGateway() {
        try {
            this.gatewayServer.start();
        } catch (Exception ex) {
            if (ex.getCause() instanceof BindException) {
                String title = "Fehler beim Starten des Gateways.";
                String details = "Fehler beim Starten des Gateways. Port "
                        + this.gatewayServer.getPort() + " oder Port "
                        + this.gatewayServer.getPythonPort() + " wird bereits verwendet.";
                String ok = "OK";

                AbstractAction acceptAction = new AbstractAction(ok) {

                    @Override
                    public void onAction() {
                    }
                };

                // show dialog
                DialogFactory.createSimpleDialog(title, title, details, acceptAction).show();
            } else {
                throw ex;
            }
        }
    }

    private void run() {
        if ((this.includeJavagateway.isSelected()
                && this.gatewayRunning.get())
                || !this.includeJavagateway.isSelected()) {
            this.gatewayMessagesList.getItems().clear();
            this.errorFunctionIds.clear();

            try {
                Runtime.getRuntime().exec(this.pythonTextField.getText() + " " + this.generateTextField.getText());
            } catch (IOException ex) {
                String title = "Fehler beim Starten des Skripts.";
                String details = "Fehler beim Starten des Skripts (" + ex.getMessage() + ").";
                String ok = "OK";

                AbstractAction acceptAction = new AbstractAction(ok) {

                    @Override
                    public void onAction() {
                    }
                };

                // show dialog
                DialogFactory.createSimpleDialog(title, title, details, acceptAction).show();
            }
        } else {
            System.out.println("Gateway not running");
        }
    }

    private void generate() {
        PrintWriter writer = null;
        // TODO folder of file exist

        try {
            /*
             * generate
             */
            ProgramFactory programFactory = new ProgramFactory();
            int size = this.workspaceController.getWorkspace().getLeafes().size();

            Program program;
            if (this.generateLoop.isSelected()) {
                program = programFactory.createLoop(this.includeJavagateway.isSelected(), this.workspaceController.getWorkspace().getLeafes().toArray(new Evaluable[size]));
            } else {
                program = programFactory.create(this.includeJavagateway.isSelected(), this.workspaceController.getWorkspace().getLeafes().toArray(new Evaluable[size]));
            }

            /*
             * write
             */
            File file = new File(this.generateTextField.getText());
            writer = new PrintWriter(file);

            // imports
            for (Import i : program.getImports()) {
                writer.println(i.getDefiningCode().getCode());
            }

            writer.println("");
            // scripts
            for (LineOfCode i : program.preCode()) {
                writer.println(i.getCode());
            }

            writer.println("");
            //lines of code
            for (LineOfCode line : program.getLinesOfCode()) {
                writer.println(line.getCode());
            }

            writer.close();
        } catch (Exception ex) {
            String title = "Fehler beim Erstellen des Skripts.";
            String details = "Fehler beim Erstellen des Skripts (" + ex.getMessage() + ").";
            String ok = "OK";

            AbstractAction acceptAction = new AbstractAction(ok) {

                @Override
                public void onAction() {
                }
            };

            // show dialog
            DialogFactory.createSimpleDialog(title, title, details, acceptAction).show();
        } finally {
            if (null != writer) {
                writer.close();
            }
        }
    }

    private void initPreferences() {
        /*
         * python
         */
        if (this.preferencesManager.contains(PreferencesManager.KEY_PYTHON_PATH)) {
            File python = new File(this.preferencesManager.get(PreferencesManager.KEY_PYTHON_PATH));

            if (python.isFile()) {
                this.pythonTextField.setText(python.getAbsolutePath());
            }
        }

        /*
         * generate
         */
        if (this.preferencesManager.contains(PreferencesManager.KEY_GENERATE_PATH)) {
            File python = new File(this.preferencesManager.get(PreferencesManager.KEY_GENERATE_PATH));

            this.generateTextField.setText(python.getAbsolutePath());
        }

        /*
         * gateway
         */
        if (this.preferencesManager.contains(PreferencesManager.KEY_INCLUDE_JAVA_GATEWAY)) {
            boolean include = Boolean.parseBoolean(this.preferencesManager.get(PreferencesManager.KEY_INCLUDE_JAVA_GATEWAY));

            this.includeJavagateway.selectedProperty().set(include);
        }

        this.includeJavagateway.selectedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            this.preferencesManager.put(PreferencesManager.KEY_INCLUDE_JAVA_GATEWAY, "" + newValue);
        });
    }

    private void initDragNDrop() {
        // detected: start drag
        this.functionHeaders.setOnDragDetected((MouseEvent event) -> {
            Dragboard db = workspaceController.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.putString("" + this.functionHeaders.getSelectionModel().getSelectedIndex());

            db.setContent(content);
            event.consume();
        });

        // over: accept drag
        this.workspaceController.setOnDragOver((DragEvent event) -> {
            event.acceptTransferModes(TransferMode.MOVE);
            event.consume();
        });

        // dropped: add function
        this.workspaceController.setOnDragDropped((DragEvent event) -> {
            addFunctionToWorkspace(this.functionHeaders.getSelectionModel().getSelectedItem(), event.getX(), event.getY());
            getStage().getScene().setCursor(Cursor.DEFAULT);

            event.setDropCompleted(true);

            event.consume();
        });
    }

    private void onGateStatusChanged(GatewayListener listener) {
        switch (listener.getStatusProperty().get()) {
            case GatewayListener.STATUS_SERVER_STARTED:
                this.gatewayStatusColor.setBackground(new Background(new BackgroundFill(Color.GREENYELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
                this.gatewayStatus.setText("Server stated");
                this.pythonGatewayPort.setText("" + this.gatewayServer.getPythonPort());
                this.javaGatewayPort.setText("" + this.gatewayServer.getPort());
                this.startGateway.setDisable(true);
                break;
            case GatewayListener.STATUS_SERVER_STOPPED:
                this.gatewayStatusColor.setBackground(new Background(new BackgroundFill(Color.GREENYELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
                this.gatewayStatus.setText("Server stopped");
                this.pythonGatewayPort.setText("");
                this.javaGatewayPort.setText("");
                this.startGateway.setDisable(false);
                break;
            case GatewayListener.STATUS_SERVER_ERROR:
                this.gatewayStatusColor.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
                this.gatewayStatus.setText("Server error: " + listener.getException());
                break;
            case GatewayListener.STATUS_CONNECTION_ERROR:
                this.gatewayStatusColor.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
                this.gatewayStatus.setText("Connection error: " + listener.getException());
                break;
            case GatewayListener.STATUS_CONNECTION_STARTED:
                this.gatewayStatusColor.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
                this.gatewayStatus.setText("Python running");
                break;
            case GatewayListener.STATUS_CONNECTION_STOPPED:
                this.gatewayStatusColor.setBackground(new Background(new BackgroundFill(Color.GREENYELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
                this.gatewayStatus.setText("Python execution completed");
                break;
        }
    }

    private void initGateway() {
        this.getStage().setOnCloseRequest((WindowEvent event) -> {
            this.gatewayServer.shutdown();
        });

        EntryPoint entryPoint = new EntryPoint();
        this.gatewayMessagesList.setItems(entryPoint.getCommunicator().getMessages());

        this.gatewayServer = new GatewayServer(entryPoint);

        GatewayListener listener = new GatewayListener();
        listener.getStatusProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            Platform.runLater(() -> {
                onGateStatusChanged(listener);
            });
        });

        this.gatewayRunning = listener.getRunningProperty();
        this.errorFunctionIds = entryPoint.getCommunicator().getErrorIds();
        this.errorFunctionIds.addListener((ListChangeListener.Change<? extends Integer> c) -> {
            this.workspaceController.unmarkErrorFunctions();

            for (Integer i : this.errorFunctionIds) {
                if (null != i) {
                    this.workspaceController.markErrorFunction(i);
                }
            }
        });

        this.gatewayServer.addListener(listener);
    }

    @Override
    public void onPostInitialization() {
        initFunctionHeadersTable();
        initDragNDrop();
        initFileChooser();
        initPreferences();
        initGateway();

        this.workspaceController.loadWorkspace(new Workspace());
    }

    @Override
    public void onPostLoad() {
        loadFunctionHeaders();
    }

    @Override
    public void onReset() {
    }
}
