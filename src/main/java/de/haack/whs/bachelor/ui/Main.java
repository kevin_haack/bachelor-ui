package de.haack.whs.bachelor.ui;

import de.haack.fxviewlibrary.view.ViewManager;
import de.haack.fxviewlibrary.view.dialog.DialogFactory;
import de.haack.whs.bachelor.ui.view.MainViewController;
import java.util.Locale;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author robin
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        /*
         * setup DialogFactory
         */
        DialogFactory.setCssPath(this.getClass().getResource("/style/general.css").toExternalForm());
        
        /*
         * setup view
         */
        ViewManager.setLocale(Locale.GERMAN);
        ViewManager.setPrimaryStage(primaryStage);
        ViewManager.loadMainView(MainViewController.class, null);
        primaryStage.show();
        primaryStage.setMaximized(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
