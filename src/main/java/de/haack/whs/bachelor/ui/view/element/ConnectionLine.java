package de.haack.whs.bachelor.ui.view.element;

import javafx.beans.binding.DoubleBinding;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;

/**
 *
 * @author Kevin Haack
 */
public class ConnectionLine extends Line {

    private OutputController outputController;
    private InputController inputController;

    public ConnectionLine(OutputController outputController) {
        this.outputController = outputController;
        
        this.setEndX(ConnectionLine.getCenterX(outputController).get());
        this.setEndY(ConnectionLine.getCenterY(outputController).get());
        
        startXProperty().bind(ConnectionLine.getCenterX(outputController));
        startYProperty().bind(ConnectionLine.getCenterY(outputController));
        
        setStrokeWidth(3);
    }

    public ConnectionLine(InputController inputController) {
        this.inputController = inputController;
        
        this.setEndX(ConnectionLine.getCenterX(inputController).get());
        this.setEndY(ConnectionLine.getCenterY(inputController).get());
        
        startXProperty().bind(ConnectionLine.getCenterX(inputController));
        startYProperty().bind(ConnectionLine.getCenterY(inputController));
        
        setStrokeWidth(3);
    }

    public ConnectionLine(OutputController outputController, InputController inputController) {
        super(0, 0, 200, 200);

        this.outputController = outputController;
        startXProperty().bind(ConnectionLine.getCenterX(outputController));
        startYProperty().bind(ConnectionLine.getCenterY(outputController));

        this.inputController = inputController;
        endXProperty().bind(ConnectionLine.getCenterX(inputController));
        endYProperty().bind(ConnectionLine.getCenterY(inputController));

        setStrokeWidth(3);
    }

    public OutputController getOutputController() {
        return outputController;
    }

    public InputController getInputController() {
        return inputController;
    }

    private static DoubleBinding getCenterX(Pane output) {
        return output.layoutXProperty()
                .add(output.getParent().getParent().layoutXProperty())
                .add(output.getParent().layoutXProperty())
                .add(output.widthProperty().divide(2.0));
    }

    private static DoubleBinding getCenterY(Pane output) {
        return output.layoutYProperty()
                .add(output.getParent().getParent().layoutYProperty())
                .add(output.getParent().layoutYProperty())
                .add(output.heightProperty().divide(2.0));
    }

    public boolean hasInputController() {
        return null != this.inputController;
    }

    public boolean hasOutputController() {
        return null != this.outputController;
    }
}
