package de.haack.whs.bachelor.ui.view.element;

import de.haack.fxviewlibrary.view.ViewManager;
import de.haack.fxviewlibrary.view.annotation.ElementController;

/**
 *
 * @author Kevin
 */
@ElementController(
        FXML = "/fxml/element/Input.fxml"
)
public class InputController extends Connectable {

    private final FunctionController functionController;
    private ConnectionLine connectionLine;

    public InputController(FunctionController functionController) {
        ViewManager.loadElementView(InputController.class, this);

        this.functionController = functionController;
    }

    public FunctionController getFunctionController() {
        return functionController;
    }

    public ConnectionLine getConnectionLine() {
        return connectionLine;
    }

    public void setConnectionLine(ConnectionLine connectionLine) {
        this.connectionLine = connectionLine;
    }

    public boolean hasConnectionLine() {
        return null != this.connectionLine;
    }
}
