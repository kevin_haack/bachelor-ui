package de.haack.whs.bachelor.ui.view.element;

import de.haack.bachelor.backend.Point;
import de.haack.bachelor.backend.Workspace;
import de.haack.bachelor.backend.script.function.Function;
import de.haack.bachelor.backend.script.function.FunctionFactory;
import de.haack.bachelor.backend.script.function.FunctionHeader;
import de.haack.fxviewlibrary.view.AbstractAction;
import de.haack.fxviewlibrary.view.ViewManager;
import de.haack.fxviewlibrary.view.annotation.ElementController;
import de.haack.fxviewlibrary.view.dialog.DialogFactory;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;

/**
 * FXML Controller class
 *
 * @author Kevin Haack
 */
@ElementController(
        FXML = "/fxml/element/Workspace.fxml"
)
public class WorkspaceController extends AnchorPane {

    private ConnectionLine newLine;
    private Rectangle select;

    private Workspace workspace;

    public WorkspaceController() throws IOException {
        ViewManager.loadElementView(WorkspaceController.class, this);
    }

    private Set<FunctionController> getAllFunctionController() {
        Set<FunctionController> functions = new HashSet<>();

        for (Node node : getChildren()) {
            if (node instanceof FunctionController) {
                functions.add((FunctionController) node);
            }
        }

        return functions;
    }

    private Set<FunctionController> getSelectedFunctions() {
        Set<FunctionController> selected = new HashSet<>();

        for (FunctionController node : getAllFunctionController()) {
            if (node.getStyleClass().contains("marked")) {
                selected.add((FunctionController) node);
            }
        }

        return selected;
    }

    public void deleteSelected() {
        for (FunctionController f : getSelectedFunctions()) {
            workspace.removeFunction(f.getFunction());
        }

        // rebuild
        loadWorkspace(workspace);
    }

    public void selectAll() {
        for (Node node : getAllFunctionController()) {
            if (!node.getStyleClass().contains("marked")) {
                node.getStyleClass().add("marked");
            }

        }
    }

    public void loadWorkspace(Workspace workspace) {
        getChildren().clear();
        this.newLine = null;
        this.select = null;

        this.workspace = workspace;

        for (Function function : workspace.getLeafes()) {
            loadFunctions(function);
        }

        for (Function function : workspace.getLeafes()) {
            loadConnections(function);
        }

        initMouseEvents();
    }

    private void unmarkOthers() {
        unmarkOthers(null);
    }

    private void unmarkOthers(FunctionController f) {
        for (Node node : this.getChildren()) {
            if (node instanceof FunctionController) {
                if (!node.equals(f)) {
                    node.getStyleClass().remove("marked");
                }
            } else {
                node.getStyleClass().remove("marked");
            }
        }
    }

    private void mark(double x, double y, double width, double height, boolean isControlDown) {
        if (!isControlDown) {
            unmarkOthers();
        }

        for (Node node : getChildren()) {
            boolean inRange = false;

            if (node instanceof FunctionController) {
                FunctionController f = (FunctionController) node;

                if (f.getLayoutX() > x
                        && f.getLayoutX() + f.getWidth() < x + width
                        && f.getLayoutY() > y
                        && f.getLayoutY() + f.getHeight() < y + height) {
                    inRange = true;
                }
            } else if (node instanceof ConnectionLine) {

            }

            if (inRange) {
                if (!node.getStyleClass().contains("marked")) {
                    node.getStyleClass().add("marked");
                } else {
                    node.getStyleClass().remove("marked");
                }
            }
        }
    }

    private void initMouseEvents() {
        /*
         * workspace
         */
        final Point selectPoint = new Point();
        setOnMouseMoved((MouseEvent event) -> {
            // new line
            if (null != this.newLine) {
                this.newLine.setEndX(event.getX());
                this.newLine.setEndY(event.getY() + 5);
            }
        });

        setOnMousePressed((MouseEvent event) -> {
            // cancel new line
            if (null != this.newLine) {
                getChildren().remove(this.newLine);
                this.newLine = null;
            }

            // start select
            if (null == this.select) {
                selectPoint.x = event.getX();
                selectPoint.y = event.getY();
                this.select = new Rectangle(event.getX(), event.getY(), 0, 0);
                this.select.getStyleClass().add("selectRect");

                getChildren().add(this.select);
            }
        });

        setOnMouseReleased((MouseEvent event) -> {
            if (null != this.select) {
                mark(this.select.getX(), this.select.getY(), this.select.getWidth(), this.select.getHeight(), event.isControlDown());

                getChildren().remove(this.select);
                this.select = null;
            }
        });

        setOnMouseDragged((MouseEvent event) -> {
            // select
            if (null != this.select) {
                double newWidth = event.getX() - this.select.getX();
                double newHeight = event.getY() - this.select.getY();

                if (selectPoint.x >= event.getX()) {
                    this.select.setX(event.getX());
                    this.select.setWidth(this.select.getWidth() + (newWidth * -1));
                } else {
                    this.select.setWidth(newWidth);
                }

                if (selectPoint.y >= event.getY()) {
                    this.select.setY(event.getY());
                    this.select.setHeight(this.select.getHeight() + (newHeight * -1));
                } else {
                    this.select.setHeight(newHeight);
                }
            }
        });

        /*
         * childs
         */
        for (Node child : getChildren()) {
            if (child instanceof FunctionController) {
                FunctionController functionController = (FunctionController) child;
                final Point dragPoint = new Point();

                // pressed
                functionController.setOnMousePressed((MouseEvent mouseEvent) -> {
                    if (null == newLine) {
                        dragPoint.x = mouseEvent.getX();
                        dragPoint.y = mouseEvent.getX();
                        functionController.getScene().setCursor(Cursor.CLOSED_HAND);

                        /*
                         * mark/unmark
                         */
                        if (functionController.getStyleClass().contains("marked")) {
                            functionController.getStyleClass().remove("marked");
                        } else {
                            functionController.getStyleClass().add("marked");
                        }

                        // unmark
                        if (!mouseEvent.isControlDown()) {
                            unmarkOthers(functionController);
                        }

                        mouseEvent.consume();
                    }
                });

                // released
                functionController.setOnMouseReleased((MouseEvent mouseEvent) -> {
                    try {
                        functionController.getScene().setCursor(Cursor.HAND);
                    } catch (Exception ex) {
                        // do nothing, this fixed a javafx bug
                    }
                    mouseEvent.consume();
                });

                // drag
                functionController.setOnMouseDragged((MouseEvent mouseEvent) -> {
                    /*
                     * mark the moved one
                     */
                    if (!functionController.getStyleClass().contains("marked")) {
                        functionController.getStyleClass().add("marked");
                    }

                    /*
                     * move
                     */
                    double oldX = functionController.getLayoutX();
                    double oldY = functionController.getLayoutY();

                    double x = oldX + mouseEvent.getX();
                    double y = oldY + mouseEvent.getY();

                    double newX = x - dragPoint.x;
                    double newY = y - dragPoint.y;

                    // Snap to grid
                    newX = newX - (newX % 20);
                    newY = newY - (newY % 20);

                    // check position
                    if (newX < 0) {
                        newX = 0;
                    }

                    if (newY < 0) {
                        newY = 0;
                    }

                    functionController.relocate(newX, newY);
                    functionController.getFunction().getPosition().x = newX;
                    functionController.getFunction().getPosition().y = newY;

                    mouseEvent.consume();

                    /*
                     * move other selected too
                     */
                    double xDifference = oldX - newX;
                    double yDifference = oldY - newY;

                    for (Node n : getChildren()) {
                        if (n instanceof FunctionController
                                && !n.equals(child)
                                && n.getStyleClass().contains("marked")) {
                            FunctionController f = (FunctionController) n;

                            double newOtherX = f.getLayoutX() - xDifference;
                            double newOtherY = f.getLayoutY() - yDifference;

                            if (newOtherX < 0) {
                                newOtherX = 0;
                            }
                            if (newOtherY < 0) {
                                newOtherY = 0;
                            }

                            f.relocate(newOtherX, newOtherY);
                            f.getFunction().getPosition().x = newOtherX;
                            f.getFunction().getPosition().y = newOtherY;
                        }
                    }
                });

                // enter
                functionController.setOnMouseEntered((MouseEvent mouseEvent) -> {
                    if (!mouseEvent.isPrimaryButtonDown()) {
                        functionController.getScene().setCursor(Cursor.HAND);
                    }

                    mouseEvent.consume();
                });

                // exit
                functionController.setOnMouseExited((MouseEvent mouseEvent) -> {
                    if (!mouseEvent.isPrimaryButtonDown()) {
                        functionController.getScene().setCursor(Cursor.DEFAULT);
                    }

                    mouseEvent.consume();
                });
            }
        }
    }

    private FunctionController getFunctionController(Function function) {
        FunctionController functionController = null;

        for (Node node : getChildren()) {
            if (node instanceof FunctionController) {
                FunctionController f = (FunctionController) node;

                if (f.getFunction().equals(function)) {
                    functionController = f;
                }
            }
        }

        return functionController;
    }

    public boolean isFunctionControllerOnWorkspace(Function function) {
        boolean onWorkspace = false;

        for (Node node : getChildren()) {
            if (node instanceof FunctionController) {
                FunctionController f = (FunctionController) node;
                if (f.getFunction().equals(function)) {
                    onWorkspace = true;
                }
            }
        }

        return onWorkspace;
    }

    private void loadConnections(Function function) {
        for (int i = 0; i < function.getParameterCount(); i++) {
            if (null != function.getParameter(i)) {
                if (function.getParameter(i) instanceof Function) {
                    Function f = (Function) function.getParameter(i);

                    OutputController output = getFunctionController(f).getOutput();
                    InputController input = getFunctionController(function).getInputs()[i];

                    ConnectionLine connectionLine = new ConnectionLine(output, input);
                    input.setConnectionLine(connectionLine);
                    getChildren().add(connectionLine);
                    initConnectionLine(connectionLine);

                    loadConnections(f);
                }
            }
        }
    }

    private void loadFunctions(Function function) {
        FunctionController functionController = new FunctionController(function);
        getChildren().add(functionController);
        initNewConnectionLine(functionController);

        for (int i = 0; i < function.getParameterCount(); i++) {
            if (null != function.getParameter(i)) {
                Function f = (Function) function.getParameter(i);

                if (!isFunctionControllerOnWorkspace(f)) {
                    loadFunctions(f);
                }
            }
        }
    }

    public void removeFunctionController(FunctionController functionController) {
        if (getChildren().contains(functionController)) {
            // backend
            this.workspace.removeFunction(functionController.getFunction());

            // rebuild
            loadWorkspace(workspace);
        }

    }

    public void addFunction(FunctionHeader functionHeader, double x, double y) {
        // Snap to grid
        x = x - (x % 20);
        y = y - (y % 20);

        if (x < 0) {
            x = 0;
        }
        if (y < 0) {
            y = 0;
        }

        // create function
        Function function = FunctionFactory.create(functionHeader, x, y);

        // add function
        this.workspace.addFunction(function);
        loadWorkspace(this.workspace);
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public void unmarkErrorFunctions() {
        for (FunctionController f : getAllFunctionController()) {
            f.getStyleClass().remove("error-border-red");
        }
    }

    public void markErrorFunction(int id) {
        for (FunctionController f : getAllFunctionController()) {
            if (f.getFunction().getId() == id) {
                if (!f.getStyleClass().contains("error-border-red")) {
                    f.getStyleClass().add("error-border-red");
                }
            }
        }
    }

    private void initNewConnectionLine(FunctionController functionController) {
        /*
         * output
         */
        OutputController outputController = functionController.getOutput();

        outputController.setOnMousePressed((MouseEvent event) -> {
            if (null == newLine) {
                newLine = new ConnectionLine(outputController);
                initConnectionLine(newLine);
                WorkspaceController.this.getChildren().add(newLine);
            } else if (!newLine.hasOutputController()) {
                /*
                 * backend
                 */
                int position = newLine.getInputController().getFunctionController().getInputPosition(newLine.getInputController());

                try {
                    this.workspace.setInput(newLine.getInputController().getFunctionController().getFunction(), outputController.getFunctionController().getFunction(), position);
                } catch (Exception ex) {
                    String title = "Fehler beim Erstellen der Verbindung.";
                    String details = "Fehler beim Erstellen der Verbindung (" + ex.getMessage() + ").";
                    String ok = "OK";

                    AbstractAction acceptAction = new AbstractAction(ok) {

                        @Override
                        public void onAction() {
                        }
                    };

                    // show dialog
                    DialogFactory.createSimpleDialog(title, title, details, acceptAction).show();
                }
                newLine = null;
                loadWorkspace(this.workspace);
            }

            event.consume();
        });

        /*
         * inputs
         */
        for (InputController inputController : functionController.getInputs()) {
            inputController.setOnMousePressed((MouseEvent event) -> {
                if (null == newLine) {
                    newLine = new ConnectionLine(inputController);
                    initConnectionLine(newLine);
                    WorkspaceController.this.getChildren().add(newLine);
                } else if (!newLine.hasInputController()) {
                    /*
                     * remove old connection
                     */
                    if (inputController.hasConnectionLine()) {
                        removeConnectionLine(inputController.getConnectionLine());
                    }

                    /*
                     * new connection
                     */
                    int position = inputController.getFunctionController().getInputPosition(inputController);
                    try {
                        this.workspace.setInput(inputController.getFunctionController().getFunction(), newLine.getOutputController().getFunctionController().getFunction(), position);
                    } catch (Exception ex) {
                        String title = "Fehler beim Erstellen der Verbindung.";
                        String details = "Fehler beim Erstellen der Verbindung (" + ex.getMessage() + ").";
                        String ok = "OK";

                        AbstractAction acceptAction = new AbstractAction(ok) {

                            @Override
                            public void onAction() {
                            }
                        };

                        // show dialog
                        DialogFactory.createSimpleDialog(title, title, details, acceptAction).show();
                    }
                    newLine = null;
                    loadWorkspace(workspace);
                }

                event.consume();
            });
        }
    }

    private void removeConnectionLine(ConnectionLine connectionLine) {
        if (getChildren().contains(connectionLine)) {
            FunctionController inputFunction = connectionLine.getInputController().getFunctionController();
            this.workspace.removeInput(inputFunction.getFunction(), inputFunction.getInputPosition(connectionLine.getInputController()));
        }
    }

    private void initConnectionLine(ConnectionLine connectionLine) {
        ContextMenu menu = new ContextMenu();

        MenuItem remove = new MenuItem("Entfernen");
        remove.setOnAction((ActionEvent event) -> {
            removeConnectionLine(connectionLine);
            loadWorkspace(workspace);
        });

        menu.getItems().addAll(remove);

        // open contextmenu
        setOnMouseClicked((MouseEvent event) -> {
            if (event.getButton() == MouseButton.SECONDARY) {
                menu.show(getScene().getWindow(), event.getScreenX(), event.getScreenY());
                event.consume();
            }
        });
    }
}
